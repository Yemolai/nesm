require('../../config/env');

const {
  DATABASE_CONNECTION_URI: connectionString,
  DATABASE_SCHEMA: database,
  DATABASE_USERNAME: username,
  DATABASE_PASSWORD: password,
  DATABASE_HOST: host,
  DATABASE_DIALECT: dialect,
  DATABASE_PORT: port
} = process.env

const config = {
  connectionString,
  username,
  password,
  database,
  host,
  dialect,
  port
}

module.exports = {
  development: config,
  test: config,
  production: config
}
