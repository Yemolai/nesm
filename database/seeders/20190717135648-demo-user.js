'use strict';

module.exports = {
  up: async (queryInterface, _Sequelize) => {
    require('dotenv').config();
    const bcrypt = require('bcrypt');
    const { getNow } = require('../../utils/datetime');
    const salt = bcrypt.genSaltSync(process.env.ROUNDS || 10);
    const password = await bcrypt.hash('admin123456', salt);
    return queryInterface.bulkInsert('Users', [{
      firstName: 'John',
      lastName: 'Doe',
      email: 'admin@demo.com',
      username: 'admin',
      password,
      createdAt: getNow(),
      updatedAt: getNow()
    }], {});
  },

  down: (queryInterface, _Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
