'use strict';
module.exports = async function () {
  const db = require('./models');
  await db.sequelize.authenticate()
    .catch(err => {
      console.error(`Unable to connect to the database: ${err.message}`);
      throw err;
    });
  console.log(`Connected to database`);
  return db;
};
