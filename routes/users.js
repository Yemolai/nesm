const express = require('express');
const router = express.Router();
const UserRepository = require('../data/user');

/* GET users listing. */
module.exports = (db) => {
  const userRepository = UserRepository(db);
  const nameValidation = /[a-zà-ú\s-\.\']+/i;
  const emailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const emailParamValidation = name => value => {
    if (!value || !value.length || !emailValidation.test(value)) {
      throw new Error('invalid-' + name);
    }
  }
  const textParamValidation = (name, { minLength = 1, maxLength = 512 } = {}) => value => {
    if (!value || value.length < minLength || value.length > maxLength) {
      throw new Error('invalid-' + name);
    }
  }
  const nameParamValidation = name => value => {
    console.log({ value })
    if (!value || !value.length || !nameValidation.test(value)) {
      throw new Error('invalid-' + name);
    }
  }
  const validateParams = (method, obj) => Object.keys(obj).map(k => method(k)(obj[k]))
  router.get('/', async function(req, res, _next) {
    if (!db.User) {
      res.status(500).json({ error: true, message: 'no users model' })
    }
    try {
      const { $page = 1, $limit = 10, $order: order = 'id' } = req.query;
      console.log(JSON.stringify({ params: req.query }));
      if (isNaN(Number($page)) || Math.floor(Number($page)) !== Number($page)) {
        throw new Error('invalid-page');
      }
      if (isNaN(Number($limit)) || Math.floor(Number($limit)) !== Number($limit)) {
        throw new Error('invalid-limit');
      }
      if (!/[a-z]+/i.test(order)) {
        throw new Error('invalid-order');
      }
      const limit = Number($limit);
      const page = Number($page);
      const users = await userRepository.getAll({ page, limit, order })
      res.status(200).json({ error: false, data: users });
    } catch ({ message }) {
      res.status(400).json({ error: true, message });
    }
  });

  router.post('/', async function (req, res, _next) {
    try {
      const { firstName, lastName, username, email, password } = req.body

      validateParams(emailParamValidation, { email });
      validateParams(nameParamValidation, { firstName, lastName });
      validateParams(textParamValidation, { username, password });

      const newUser = { firstName, lastName, username, email, password }
      const createdUser = await userRepository.create(newUser);

      return res.status(201).json({ error: false, data: createdUser })
    } catch (err) {
      res.status(400).json({ error: true, message: err.message })
    }
  });

  return router;
};
