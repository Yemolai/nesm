const express = require('express');
const LoginRouter = express.Router();
const UserRepository = require('../data/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { jwtOptions } = require('../utils/JwtPassportStrategy');

module.exports = (db) => {
  const userRepository = UserRepository(db);
  LoginRouter.post('/', async function(req, res, _next) {
    try {
      const { username, email, password } = req.body
      if (!password || (!username && !email)) {
        throw new Error('missing-credentials');
      }
      const userSearchParams = email ? { email } : { username }
      const user = await userRepository.get(userSearchParams)
      if (!user || !bcrypt.compareSync(password, user.password)) {
        throw new Error('invalid-credentials');
      }
      const payload = { id: user.id };
      const token = jwt.sign(payload, jwtOptions.secretOrKey);
      res.status(200).json({ error: false, data: token });
    } catch ({ message }) {
      res.status(400).json({ error: true, message })
    }
  })
  return LoginRouter;
}
