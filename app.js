const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const { JwtPassportStrategy, jwtAuthGuard } = require('./utils/JwtPassportStrategy');
const database = require('./database');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const loginRouter = require('./routes/login');

async function startApp() {
  const app = express();
  const db = await database();

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'jade');

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(JwtPassportStrategy(db));

  app.use('/api/login', loginRouter(db));
  app.use('/api/users', jwtAuthGuard, usersRouter(db));
  app.use('/api', jwtAuthGuard, indexRouter(db));

  // catch 404 and forward to error handler
  app.use(function(_req, _res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function(err, req, res, _next) {
    // set locals, only providing error in development
    const isDev = req.app.get('env') === 'development'
    const message = isDev ? err.message : 'internal error';
    console.error('error:', message)

    // render the error page
    res.status(err.status || 500);
    res.json({
      error: true,
      message: res.locals.message || 'internal error'
    });
  });
  return app;
}

module.exports = startApp;
