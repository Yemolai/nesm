// import passport and passport-jwt modules
const passport = require('passport');
const passportJWT = require('passport-jwt');

const UserRepository = require('../data/user');

// ExtractJwt to help extract the token
const ExtractJwt = passportJWT.ExtractJwt;
// JwtStrategy which is the strategy for the authentication
const JwtStrategy = passportJWT.Strategy;
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = process.env.SECRET || Math.random().toString(34).slice(2);

// consts create our strategy for web token
const strategy = db => new JwtStrategy(jwtOptions, async function(jwt_payload, next) {
  console.log('payload received', jwt_payload);
  const userRepository = UserRepository(db);
  const user = await userRepository.get({ id: jwt_payload.id });
  if (user) {
    next(null, user);
  } else {
    next(null, false);
  }
});

const JwtPassportStrategy = (db) => {
  // use the strategy
  passport.use(strategy(db));
  return passport.initialize();
}
const jwtAuthGuard = passport.authenticate('jwt', { session: false });
module.exports = { JwtPassportStrategy, jwtAuthGuard, jwtOptions }
