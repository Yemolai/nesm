const toDatetime = (date) => {
  const pad = n => Number(n).toString().padStart(2, 0)
  const Y = date.getUTCFullYear()
  const M = pad(date.getUTCMonth() + 1)
  const D = pad(date.getUTCDate())
  const h = pad(date.getUTCHours())
  const m = pad(date.getUTCMinutes())
  const s = pad(date.getUTCSeconds())
  return `${Y}-${M}-${D} ${h}:${m}:${s}`
};

module.exports = {
  toDatetime,
  getNow: () => {
    return toDatetime(new Date())
  }
}
