require('dotenv').config();
const bcrypt = require('bcrypt');
module.exports = ({ User }) => ({
  create: async ({ firstName, lastName, username, password: rawPassword , email }) => {
    // validations go Here
    const salt = bcrypt.genSaltSync(process.env.ROUNDS || 10);
    const password = await bcrypt.hash(rawPassword, salt);
    const newUser = { firstName, lastName, username, password, email };

    return await User.create(newUser)
      .then(createdUser => {
        createdUser.password = undefined;
        return createdUser;
      });
  },
  getAll: async ({ order, desc = false, limit = 100, page = 1 }) => {
    // validations and pagination goes here
    const offset = (page - 1) * limit;
    const options = { attributes: { exclude: ['password'] } };
    const hasOrder = order && typeof order === 'string'
    options.order = [[hasOrder ? order : 'id', desc ? 'ASC' : 'DESC']];
    options.limit = limit < 1 ? 1 : limit;
    options.offset = offset < 0 ? 0 : offset;
    const total = await User.count();
    const docs = await User.findAll(options);
    return ({ page: { order: `${desc ? '-': ''}${order}`, limit, current: page, total }, docs });
  },
  get: async (where) => {
    return await User.findOne({ where });
  },
  update: async (id, values) => {
    const where = { id };
    const fields = Object.keys(values);
    return await User.update(values, { where, fields });
  },
  delete: async (id) => {
    const where = { id };
    return await User.destroy(where);
  }
});
